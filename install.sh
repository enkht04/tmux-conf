#!/bin/bash
function install_it {

YUM_CMD=$(which yum)
DNF_CMD=$(which dnf)
APT_GET_CMD=$(which apt-get)
pkg_name="${1}"

if [[ ! -z "$YUM_CMD" ]]; then
    sudo "${YUM_CMD}" install -y "$pkg_name"
elif [[ ! -z "$DNF_CMD" ]]; then
     sudo "${DNF_CMD}" install -y "$pkg_name"
elif [[ ! -z "$APT_GET_CMD" ]]; then
    sudo "${APT_CMD}" install -y "$pkg_name"
else
    echo -e "\nERROR can't install package $pkg_name"
    exit 1;
fi
}

function is_installed {

if ! command -v "${1}" &> /dev/null
then
    echo "${1} could not be found"
    echo -e "\nWould you like to install ${1}? (y/n)"
    #read answer
    answer=y ##acomment to enable user input
    if [ "$answer" != "${answer#[Yy]}" ] ;then
        echo Yes
        install_it "${1}"
    else
        exit 1;
    fi
fi
}

BASEDIR=$(dirname "$0")
is_installed tmux &&

## Determine current Display Server and respective tmux dependencies
if [ "$XDG_SESSION_TYPE" = x11 ];then 
    clipboard="xclip";
elif [ "$XDG_SESSION_TYPE" = wayland ];then
    clipboard="wl-clipboard";
elif [ "$XDG_SESSION_TYPE" -z ];then
    echo "Unable to determine current Display Sever, OS clipboard support install failed"
fi    
is_installed "$clipboard" &&

## Copy configs
cp -R "$BASEDIR"/.tmux* ~/ && echo -e "Tmux config setup Succeded"
